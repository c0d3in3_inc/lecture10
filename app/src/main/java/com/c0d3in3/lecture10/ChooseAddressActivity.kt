package com.c0d3in3.lecture10

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_choose_address.*

class ChooseAddressActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_address)

        init()
    }

    private fun init(){
        chooseAddressButton.setOnClickListener {
            if(checkAddress(addressEditText.text.toString()))   chooseAddress()
            else{
                Toast.makeText(this, "Address field must not be empty or blank!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun chooseAddress(){
        intent.putExtra("address", addressEditText.text.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun checkAddress(address : String) : Boolean{
        if(address.isNotBlank() && address.isNotEmpty()) return true
        return false
    }
}
