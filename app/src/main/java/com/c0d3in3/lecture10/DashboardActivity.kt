package com.c0d3in3.lecture10

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        init()
    }

    private fun init(){
        val userModel = intent.extras!!.get("userModel") as UserModel?
        usernameTextView.text = "Username: ${userModel?.mUsername}"
        firstNameTextView.text = "First name: ${userModel?.mFirstName}"
        lastNameTextView.text = "Last name: ${userModel?.mLastName}"
        ageTextView.text = "Age: ${userModel?.mAge.toString()}"
        addressTextView.text = "Address : ${userModel?.mAddress}"
    }
}
