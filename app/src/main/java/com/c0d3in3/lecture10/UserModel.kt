package com.c0d3in3.lecture10

import android.os.Parcel
import android.os.Parcelable

class UserModel(private val username : String, private val firstName : String, private val lastName : String, private val age : Int, private val address : String) : Parcelable {

    val mUsername = this.username
    val mFirstName = this.firstName
    val mLastName = this.lastName
    val mAge = this.age
    val mAddress = this.address

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(username)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeInt(age)
        parcel.writeString(address)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserModel> {
        override fun createFromParcel(parcel: Parcel): UserModel {
            return UserModel(parcel)
        }

        override fun newArray(size: Int): Array<UserModel?> {
            return arrayOfNulls(size)
        }
    }
}