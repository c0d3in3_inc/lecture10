package com.c0d3in3.lecture10

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var username : EditText
    private lateinit var firstName : EditText
    private lateinit var lastName : EditText
    private lateinit var age : EditText
    private var address: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){
        addressTextView.setOnClickListener {
            chooseAddress()
        }
        signUpButton.setOnClickListener {
            signUp()
        }

        username = usernameEditText
        firstName = firstNameEditText
        lastName = lastNameEditText
        age = ageEditText
    }

    private fun chooseAddress(){

        val intent = Intent(this, ChooseAddressActivity::class.java)
        startActivityForResult(intent, REQUEST_ADDRESS)
    }

    private fun signUp(){
        if(checkFields()){
            val userModel = UserModel(username.text.toString(), firstName.text.toString(), lastName.text.toString(), age.text.toString().toInt(), address.toString())
            val intent = Intent(this, DashboardActivity::class.java)
            intent.putExtra("userModel", userModel)
            intent.flags = (FLAG_ACTIVITY_CLEAR_TASK or FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
        else Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()

    }

    companion object {
        const val REQUEST_ADDRESS = 22
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == RESULT_OK && requestCode == REQUEST_ADDRESS){
            addressTextView.text = data!!.getStringExtra("address")
            address = addressTextView.text.toString()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun checkFields(): Boolean {
        if(age.text.toString().isNotEmpty() && username.text.toString().isNotEmpty() && lastName.text.toString().isNotEmpty()
            && firstName.text.toString().isNotEmpty() && address != null) return true
        return false
    }
}
